﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIScript : MonoBehaviour {

    public GameObject GG;

    private Vector3 v_diff;
    private float atan2;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        v_diff = (GG.transform.position - transform.position);
        atan2 = Mathf.Atan2(v_diff.y, v_diff.x);
        transform.rotation = Quaternion.Euler(0f, 0f, atan2 * Mathf.Rad2Deg);
        var move = transform.right;
        transform.position += move * GetComponent<MovingUnitScript>().speed * Time.deltaTime;
        Debug.Log(Vector3.Distance(GG.transform.position, transform.position));
        if (Vector3.Distance(GG.transform.position, transform.position) > 6f ){
            var go = new GameObject();
            var a = go.AddComponent<Effect>();
            go.GetComponent<Effect>().coefficient = 2f;
            go.GetComponent<Effect>().Ticks = 10;
            this.GetComponent<MovingUnitScript>().AddEffect(a);
        }
    }
}
