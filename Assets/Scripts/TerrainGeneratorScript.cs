﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainGeneratorScript : MonoBehaviour {

    public GameObject GrassTemplate;
    public List<GameObject> Effects;
    public GameObject GG;   

	// Use this for initialization
	void Start () {
        for (int i = -10; i <= 10; i++)
        {
            for (int j = -10; j <= 10; j++)
            {
                Instantiate(GrassTemplate, new Vector3(i * 5, j * 5), GrassTemplate.transform.rotation);
            }
        }
        
	}
	
	// Update is called once per frame
	void Update () {
        
	    if(Random.Range(0,100) == 0)
        {
            var place = GG.transform.position;
            Debug.Log(place);
            place.x += Random.Range(0, 1) == 0 ? Random.Range(2f, 8f) : -Random.Range(2f, 8f);
            place.y += Random.Range(0, 1) == 0 ? Random.Range(2f, 8f) : -Random.Range(2f, 8f);
            Debug.Log(place);
            Instantiate(Effects[Random.Range(0, Effects.Count)], place, GG.transform.rotation);
        }
	}
}
