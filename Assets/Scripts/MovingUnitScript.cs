﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingUnitScript : MonoBehaviour {

    public float BaseSpeed = 1f;
    [HideInInspector]
    public float speed = 1f;

    List<Effect> effects = new List<Effect>();

    public void AddEffect(Effect effect)
    {
        effects.Add(effect);
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        speed = BaseSpeed;
        foreach (var effect in effects)
        {
            effect.Ticks--;
            speed *= effect.coefficient;
            Debug.Log(effect.Ticks);
        }
        effects.RemoveAll(x => x.Ticks <= 0);
        
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Effect")
        {
            var effect = collision.GetComponent<Effect>();
            AddEffect(effect);
            Destroy(collision.gameObject);
        }
    }
}
