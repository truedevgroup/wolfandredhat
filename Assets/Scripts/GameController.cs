﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        //var k = 1/(Math.Abs(Input.GetAxis("Horizontal")) + Math.Abs(Input.GetAxis("Vertical")));
        //Debug.Log("k = " + k);
        ////Debug.Log((float)Math.Sin(k)*Input.GetAxis("Horizontal") + (float)Math.Cos(k) * Input.GetAxis("Vertical"));
        //var move = new Vector3((float)Math.Cos(Math.PI / 2 * Input.GetAxis("Horizontal")*k),(float)Math.Sin(Math.PI / 2 * Input.GetAxis("Vertical") * k), 0);

        transform.Rotate(new Vector3(0, 0, -Input.GetAxis("Horizontal")));
        transform.position += transform.up * GetComponent<MovingUnitScript>().speed * Time.deltaTime;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
            Debug.Log("Ate");
    }
}
